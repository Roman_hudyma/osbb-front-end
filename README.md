features:

*   Developed using boostrap-v4.0.0
*   angular-v8.0.0
*   angular/cli-v8.0.0

### How to start

**Note** that this seed project requires **node >=v8.9.0 and npm >=4**.

In order to start the project use:

```bash
$ git clone git@bitbucket.org:Roman_hudyma/osbb-front-end.git
$ cd osbb-front-end
# install the project's dependencies
$ npm install
# watches your files and uses livereload by default run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
$ npm start
# prod build, will output the production application in `dist`
# the produced code can be deployed (rsynced) to a remote server
$ npm run build
```
